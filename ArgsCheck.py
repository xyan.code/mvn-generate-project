#!/usr/bin/ python3
from readcfg import ReadConfig as rc


class Check:
    # 初始化函数
    def __init__(self, first, protype, mgit):
        self.first = first
        self.protype = protype
        self.mgit = mgit
        self.rc = rc()

    # 参数校验
    def __check__(self):
        if self.first is None:
            self.first = "N"
        if self.protype is not None and self.mgit is not None:
            raise Exception("参数「-t」和「-g」只能选其一")

    # 获取模板git仓库地址
    def __git_addr__(self):
        if self.mgit is not None:
            return self.mgit
        else:
            if self.protype == "jpa" or self.protype is None:
                return self.rc.get("Git-Store", "jpa")
            else:
                return self.rc.get("Git-Store", "mybatis")
