#!/usr/bin/ python3

import configparser


class ReadConfig:
    def __init__(self):
        parser = configparser.ConfigParser()
        parser.read("config.ini")
        self.cfg = parser

    def get(self, section, key):
        return self.cfg.get(section, key)
