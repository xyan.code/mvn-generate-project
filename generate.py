#!/usr/bin/ python3
import click
from GenMvnProject import CreateProject
from ArgsCheck import Check


class DefaultHelp(click.Command):
    def __init__(self, *args, **kwargs):
        context_settings = kwargs.setdefault('context_settings', {})
        if 'help_option_names' not in context_settings:
            context_settings['help_option_names'] = ['-h', '--help']
        self.help_flag = context_settings['help_option_names'][0]
        super(DefaultHelp, self).__init__(*args, **kwargs)

    def parse_args(self, ctx, args):
        if not args:
            args = [self.help_flag]
        return super(DefaultHelp, self).parse_args(ctx, args)


@click.command(cls=DefaultHelp)
@click.option("--first", "-f", type=click.Choice(['y', 'Y', 'n', 'N']),
              help="是否第一次构建项目「Y/y：是，N/n：否」，提示：是，则去git仓库clone模板工程")
@click.option("--project_type", "-t", type=click.Choice(['jpa', 'mybatis']),
              help="「jpa：jpa工程|mybatis：mybatis工程」，默认是jpa")
@click.option("--custom_git_addr", "-g", type=click.STRING, help="自定义git仓库项目模板，如果不传则使用默认模板")
@click.option("--project_save_dir", "-d", prompt="请输入工程保存路径", help="工程保存路径")
def command(first, project_save_dir, project_type, custom_git_addr):
    check = Check(first, project_type, custom_git_addr)
    check.__check__()
    CreateProject(check.first, project_save_dir, check.__git_addr__()).__create_mvn_module_project__()


if __name__ == '__main__':
    command()
