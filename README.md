# mvn-generate-project

## 使用中可能遇到的问题

### 一、报错Invoker process ended with result different than 0!

分析：该错误是在执行`mvn archetype:create-from-project`命令时发生的错误，原因在于用户的.m2目录下缺少maven配置文件。

解决：在用户的`.m2`目录下添加maven配置文件`settings.xml`文件即可

### 二、mac下pyinstaller打包报错

错误描述：

come with proper library files. This usually happens by missing development package, or unsuitable build parameters of
Python installation.

* On Debian/Ubuntu, you would need to install Python development packages
    * apt-get install python3-dev
    * apt-get install python-dev
* If you're building Python by yourself, please rebuild your Python with
  `--enable-shared` (or, `--enable-framework` on Darwin)

解决：`PYTHON_CONFIGURE_OPTS="--enable-framework" pyenv install 3.9.0`
> 提示：
> 1. `3.9.0`仅仅为你的Python版本
> 2. 命令`pyenv install --list`查看所有有效版本信息

**_事实证明上面的方案依然解决不了，亲测下面的方案可以解决：_**
1. 卸载当前安装的pyinstaller
```shell
pip3 uninstall pyinstaller
```
2. 重新安装
```shell
pip3 install --upgrade https://github.com/pyinstaller/pyinstaller/tarball/develop
```
